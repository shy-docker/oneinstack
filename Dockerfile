FROM centos:6.8 as oneinstack
COPY CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo
WORKDIR /oneinstack
ADD oneinstack-full.tar.gz /
COPY install-docker.sh /oneinstack/install-docker.sh
ENV iptables_yn=n \
    Web_yn=y \
    Nginx_version=1 \
    Apache_version=3 \
    Tomcat_version=4 \
    DB_yn=n \
    PHP_yn=y PHP_version=7 \
    PHP_cache_yn=n ionCube_yn=n ZendGuardLoader_yn=n \
    Magick_yn=y Magick=1 \
    FTP_yn=n phpMyAdmin_yn=n \
    redis_yn=n memcached_yn=n HHVM_yn=n \
    restart_yn=n
RUN ./install-docker.sh || echo 'finish'
FROM centos:6.8
COPY --from=oneinstack / /
COPY nginx.conf /usr/local/nginx/conf/nginx.conf
WORKDIR /oneinstack
# 先启动 php-fpm 绑定到文件句柄上 , 之后启动 nginx 把 php 文件转发过去
CMD /usr/local/php/sbin/php-fpm && /usr/local/nginx/sbin/nginx -g "daemon off;"