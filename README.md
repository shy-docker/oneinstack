## Usage

```sh
sudo docker run -d --restart always \
  --name oneinstack \
  -p 80:80 -p 443:443 \
  -v /web/ssl/:/usr/local/nginx/conf/ssl/ \
  -v /web/vhost/:/usr/local/nginx/conf/vhost/ \
  -v /web/wwwlogs/:/data/wwwlogs/ \
  -v /web/wwwroot/:/data/wwwroot/ \
  shynome/oneinstack:latest
```
